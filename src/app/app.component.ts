import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { PagerService } from './pager.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {



  pageSizeItems: any =  [ 10, 20, 30, 40, 50 ];

  pageSize = 10;
  // employee info array. Which  holds the array of employees which are coming from the api.
  data: any;

   // pager object
   pager: any = {};

   // paged items
   pagedItems: any[];

  constructor(private dataService: DataService, private pagerService: PagerService) {  }
  title = 'Capco Angular Assessment';

  ngOnInit() {
    this.dataService.GetData()
    .subscribe(data => {
        // set items to json response
        this.data = data;

        // initialize to page 1
        this.setPage(1);
    });

  }

  setPage(page: number) {
    // get pager object from service
    this.pager = this.pagerService.getPager(this.data.length, page, this.pageSize);

    // get current page of items
    this.pagedItems = this.data.slice(this.pager.startIndex, this.pager.endIndex + 1);
}

changePageSize(value: number) {
  this.pageSize = +value;
  this.setPage(1);
}
}
