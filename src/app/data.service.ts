import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class DataService {

  private path: string;
  constructor(private httpClient: HttpClient) { this.path = 'assets/sample_data.json'; }
  GetData(): Observable<any[]> {
    return  this.httpClient.get<any[]>(this.path);
 }

}
